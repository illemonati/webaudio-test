import React, { useEffect, useState } from 'react';
import {
    Button,
    Container,
    Grid,
    Paper,
    Slider,
    TextField,
    Typography,
} from '@material-ui/core';
import { PlayState } from '../../utils/playstate';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const Home = () => {
    const [fileUrl, setFileUrl] = useState(
        'http://www.hochmuth.com/mp3/Beethoven_12_Variation.mp3'
    );
    //eslint-disable-next-line
    const [audioCtx, setAudioCtx] = useState(new AudioContext());
    const [currentTime, setCurrentTime] = useState(0);
    const [playState, setPlayState] = useState(PlayState.Uninitialized);
    //eslint-disable-next-line
    const [gainNode, setGainNode] = useState(audioCtx.createGain());
    //eslint-disable-next-line
    const [audioElement, setAudioElement] = useState(new Audio());
    const [volume, setVolume] = useState(1);
    const pannerOptions = { pan: 0 };
    //eslint-disable-next-line
    const [panner, setPanner] = useState(
        new StereoPannerNode(audioCtx, pannerOptions)
    );
    const [stereoPan, setStereoPan] = useState(0);

    const handleClick = () => {
        setPlayState(PlayState.Paused);
        audioElement.src = 'https://cors-anywhere.herokuapp.com/' + fileUrl;
        audioElement.crossOrigin = 'anonymous';
        setPlayState(PlayState.Paused);
    };

    const secondsToHHMMSSString = (seconds: number) => {
        const dateObj = new Date(seconds * 1000);
        const hours = dateObj.getUTCHours();
        const minutes = dateObj.getUTCMinutes();
        const nseconds = dateObj.getSeconds();

        const timeString =
            hours.toString().padStart(2, '0') +
            ':' +
            minutes.toString().padStart(2, '0') +
            ':' +
            nseconds.toString().padStart(2, '0');
        return timeString;
    };

    useEffect(() => {
        const src = audioCtx.createMediaElementSource(audioElement);
        src.connect(gainNode).connect(panner).connect(audioCtx.destination);
        audioElement.ontimeupdate = function () {
            setCurrentTime(audioElement.currentTime);
        };
        //eslint-disable-next-line
    }, []);

    useEffect(() => {
        gainNode.gain.value = volume;
    }, [gainNode, volume]);

    useEffect(() => {
        panner.pan.value = stereoPan;
    }, [stereoPan, panner]);

    useEffect(() => {
        if (audioCtx && audioElement) {
            console.log(audioCtx.state);
            if (playState === PlayState.Playing) {
                audioElement
                    .play()
                    .then()
                    .catch((e) => console.log(e));
            } else if (playState === PlayState.Paused) {
                audioElement.pause();
            }
        }
        //eslint-disable-next-line
    }, [playState]);

    return (
        <div className="Home">
            <Container>
                <Typography variant="h3">Web Audio Context Demo</Typography>
                <br />
                <br />
                <Paper>
                    <Container>
                        <Grid
                            container
                            justify="center"
                            alignItems="center"
                            spacing={3}
                        >
                            <Grid item xs={9}>
                                <TextField
                                    label="file url"
                                    fullWidth
                                    value={fileUrl}
                                    onChange={(e) => {
                                        setFileUrl(e.target.value);
                                    }}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <Button
                                    onClick={handleClick}
                                    variant="contained"
                                >
                                    Load
                                </Button>
                            </Grid>
                        </Grid>
                    </Container>
                </Paper>
                <br />
                <br />
                <br />
                <br />

                {playState !== PlayState.Uninitialized && (
                    <Paper>
                        <Container>
                            <Grid
                                container
                                justify="center"
                                alignItems="center"
                                spacing={3}
                            >
                                <Grid item xs={4}>
                                    <Button
                                        variant="contained"
                                        onClick={(e) => {
                                            setPlayState(PlayState.Playing);
                                            audioCtx.resume();
                                        }}
                                        style={{ backgroundColor: '#4CAF50' }}
                                    >
                                        Play
                                    </Button>
                                </Grid>
                                <Grid item xs={4}>
                                    <Button
                                        variant="contained"
                                        onClick={(e) =>
                                            setPlayState(PlayState.Paused)
                                        }
                                        style={{ backgroundColor: '#FFC107' }}
                                    >
                                        Pause
                                    </Button>
                                </Grid>
                                <Grid item xs={4}>
                                    <Button
                                        variant="contained"
                                        onClick={(e) => {
                                            setPlayState(PlayState.Paused);
                                            audioElement.currentTime = 0;
                                        }}
                                        style={{ backgroundColor: '#F44336' }}
                                    >
                                        Stop
                                    </Button>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography gutterBottom>Time</Typography>
                                </Grid>
                                <Grid item xs={1}>
                                    <Typography>
                                        {secondsToHHMMSSString(currentTime)}
                                    </Typography>
                                </Grid>
                                <Grid item xs={10}>
                                    <Slider
                                        value={currentTime}
                                        max={audioElement.duration}
                                        min={0}
                                        step={0.01}
                                        onChange={(e, v) => {
                                            audioElement.currentTime = v as number;
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <Typography>
                                        {audioElement.duration
                                            ? secondsToHHMMSSString(
                                                  audioElement.duration
                                              )
                                            : '00:00:00'}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography gutterBottom>Volume</Typography>
                                </Grid>
                                <Grid item xs={1}>
                                    <VolumeDown />
                                </Grid>
                                <Grid item xs>
                                    <Slider
                                        value={volume}
                                        max={2}
                                        min={0}
                                        step={0.01}
                                        onChange={(e, v) =>
                                            setVolume(v as number)
                                        }
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <VolumeUp />
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography gutterBottom>
                                        Stereo Panning
                                    </Typography>
                                </Grid>
                                <Grid item xs={1}>
                                    <ArrowBackIcon />
                                </Grid>
                                <Grid item xs>
                                    <Slider
                                        value={stereoPan}
                                        color="secondary"
                                        max={1}
                                        min={-1}
                                        step={0.01}
                                        onChange={(e, v) =>
                                            setStereoPan(v as number)
                                        }
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <ArrowForwardIcon />
                                </Grid>
                            </Grid>
                        </Container>
                    </Paper>
                )}
            </Container>
        </div>
    );
};

export default Home;
